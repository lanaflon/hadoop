package com.intellitech.training.keyword;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class KeywordsReducer extends Reducer<Text, Text, Text, Text> {

	@Override
	protected void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, Text, Text>.Context context)
			throws IOException, InterruptedException {

		StringBuilder sb = new StringBuilder();
		for (Text value : values) {
			sb.append(value);
		}
		sb.setLength(sb.length() - 1);
		context.write(key, new Text(sb.toString()));
	}
}
