package com.intellitech.training.keyword;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class KeywordsMapper extends Mapper<Text, Text, Text, Text> {

	@Override
	protected void map(Text key, Text values, Mapper<Text, Text, Text, Text>.Context context)
			throws IOException, InterruptedException {

		for (String value : values.toString().split(",")) {
			context.write(new Text(value), key);
		}
	}
}
