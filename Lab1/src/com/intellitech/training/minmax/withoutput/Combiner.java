package com.intellitech.training.minmax.withoutput;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

// the role of combiner is to do the same work of the reducer
// but on the ouput of each mapper before shuffling
// so to minimize the transfer of data via network
public class Combiner 
		extends Reducer<IntWritable, IntWritable, IntWritable, IntWritable>{

	@Override
	protected void reduce(IntWritable key, Iterable<IntWritable> values,
			Reducer<IntWritable, IntWritable, IntWritable, IntWritable>.Context context)
					throws IOException, InterruptedException {
		//perform the same action of the reducer
		int max = values.iterator().next().get();
		int min = max;
		for(IntWritable value : values){
			if(value.get() > max){
				max = value.get();
			}
			if(value.get() <  min){
				min = value.get();
			}
		}
		context.write( new IntWritable(1), new IntWritable(min));
		context.write( new IntWritable(1), new IntWritable(max));
		
		
	}
}
