package com.intellitech.training.minmax.nooutput;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

// Each reducer should implements Reducer<KeyInputType,ValueInputType,KeyOutputType,ValueOutputType>
public class MinMaxReducer 
	extends Reducer<IntWritable,IntWritable,NullWritable,NullWritable>{
	
	// Override the reduce method to implement the process
	// That should be done by the reducer
	
	// Because we will write to counters not to file, we should set the 
	// output key and value to NullWritable.
	@Override
	protected void reduce(IntWritable key, Iterable<IntWritable> values,
			Reducer<IntWritable, IntWritable, NullWritable, NullWritable>.Context context)
					throws IOException, InterruptedException {
		// on each key 
		// this method will be called with the key and the list of values
		
		// set the first value as min and max
		int max = values.iterator().next().get();
		int min = max;
		
		// loop for other values and compare them each time to the min and max
		for(IntWritable value : values){
			if(value.get() > max){
				max = value.get();
			}
			if(value.get() <  min){
				min = value.get();
			}
		}
		
		//write values to counters instead of context.write ...
		context.getCounter("result", "min").setValue(min);
		context.getCounter("result", "max").setValue(max);
	}

}
