package com.intellitech.training.exam;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.intellitech.training.exam.input.FlightInput;
import com.intellitech.training.exam.input.WeatherInput;
import com.intellitech.training.exam.util.GroupComparator;
import com.intellitech.training.exam.util.WeatherDelayPartionner;

public class WheatherDelayJob extends Configured implements Tool {

	@Override
	public int run(String[] arg0) throws Exception {
		Configuration conf = getConf();
		Job job =  Job.getInstance(conf);
		Path inputPath = new Path("hdfs://localhost:9000/hortonworks/input/flight_delays*.csv");
		Path inputPath2 = new Path("hdfs://localhost:9000/hortonworks/input/sfo_weather.csv");
		Path outputDir = new Path("hdfs://localhost:9000/hortonworks/output");
		MultipleInputs.addInputPath(job, inputPath, FlightInput.class, FlightMapper.class);
		MultipleInputs.addInputPath(job, inputPath2, WeatherInput.class, WeatherMapper.class);
		job.setReducerClass(FlightReducer.class);
		
		job.setGroupingComparatorClass(GroupComparator.class);
		job.setPartitionerClass(WeatherDelayPartionner.class);
		job.setNumReduceTasks(2);
		TextOutputFormat.setOutputPath(job, outputDir);
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setOutputKeyClass(FlightDate.class);
		job.setOutputValueClass(Text.class);
		outputDir.getFileSystem(conf).delete(outputDir, true);
		
		return job.waitForCompletion(true)? 0: 1;
	}

	public static void main(String[] args) throws Exception {
		ToolRunner.run(new WheatherDelayJob(), args);

	}

}
