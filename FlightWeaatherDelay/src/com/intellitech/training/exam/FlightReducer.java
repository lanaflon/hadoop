package com.intellitech.training.exam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class FlightReducer extends Reducer<FlightDate, Text, FlightDate, Text> {

	@Override
	protected void reduce(FlightDate key, Iterable<Text> values, Reducer<FlightDate, Text, FlightDate, Text>.Context context)
			throws IOException, InterruptedException {
		String weather ="";
		List<String> flighs  = new ArrayList<>();
		
		for(Text value : values){
			if(value.toString().startsWith("wthr:")){
				weather = value.toString().substring(5);
			}
			if(value.toString().startsWith("dlay:")){
				flighs.add(value.toString().substring(5));
			}
			
		}
		for(String flight : flighs){
			System.err.println("--> flight " + flight);
			context.write(key, new Text(flight + "," + weather ));
		}
	}

}
