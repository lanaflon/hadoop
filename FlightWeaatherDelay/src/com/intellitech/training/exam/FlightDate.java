package com.intellitech.training.exam;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;

public class FlightDate implements WritableComparable<FlightDate> {
	IntWritable day;
	IntWritable month;
	IntWritable year;
	IntWritable arrDelay;

	public FlightDate(int day, int month, int year, int arrDelay) {
		this.day=  new IntWritable(day);
		this.month=  new IntWritable(month);
		this.year=  new IntWritable(year);
		this.arrDelay=  new IntWritable(arrDelay);
	}
	
	public FlightDate(){
		this.day=  new IntWritable();
		this.month=  new IntWritable();
		this.year=  new IntWritable();
		this.arrDelay=  new IntWritable();
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		day.readFields(in);
		month.readFields(in);
		year.readFields(in);
		arrDelay.readFields(in);
		
	}

	@Override
	public void write(DataOutput out) throws IOException {
		day.write(out);
		month.write(out);
		year.write(out);
		arrDelay.write(out);
		
	}

	@Override
	public int compareTo(FlightDate o) {
		
		int codeComp = year.compareTo(o.getYear());
		if(codeComp != 0){
			return codeComp;
		}else{
			codeComp = month.compareTo(o.getMonth());
			if(codeComp != 0){
				return codeComp;
			}else{
				codeComp = day.compareTo(o.getDay());
				if(codeComp != 0){
					return codeComp;
				}else{
					codeComp = -arrDelay.compareTo(o.getArrDelay());
					return codeComp;			
				}
				
			}
		}

	}

	public IntWritable getDay() {
		return day;
	}

	public void setDay(IntWritable day) {
		this.day = day;
	}

	public IntWritable getMonth() {
		return month;
	}

	public void setMonth(IntWritable month) {
		this.month = month;
	}

	public IntWritable getYear() {
		return year;
	}

	public void setYear(IntWritable year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "DelayDate [day=" + day + ", month=" + month + ", year=" + year + "]";
	}

	public IntWritable getArrDelay() {
		return arrDelay;
	}

	public void setArrDelay(IntWritable arrDelay) {
		this.arrDelay = arrDelay;
	}

}
