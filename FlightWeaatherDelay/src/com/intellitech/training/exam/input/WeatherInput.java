package com.intellitech.training.exam.input;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import com.intellitech.training.exam.FlightDate;

public class WeatherInput extends FileInputFormat<FlightDate, Text> {

	@Override
	public RecordReader<FlightDate, Text> createRecordReader(InputSplit split, TaskAttemptContext context)
			throws IOException, InterruptedException {
		WeatherReader  dr = new WeatherReader();
		dr.initialize(split, context);
		return dr;
	}

}
