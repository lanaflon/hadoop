package com.intellitech.healthcare.cheapestProvider;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class ProcedureCheapestProviderPartitionner extends Partitioner<Text, Text> {

	@Override
	public int getPartition(Text key, Text value, int numPartitions) {
		Long keyLong = 0L; keyLong = Long.parseLong(key.toString().split(" - ")[0]);
		if(keyLong < 500)
			return 0;
		else 
			return 2;
	}

}
