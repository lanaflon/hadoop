package com.intellitech.healthcare.cheapestProvider;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class ProcedureCheapestProviderCombiner extends Reducer<Text, Text, Text, Text> {

	@Override
	protected void reduce(Text key, Iterable<Text> values,
			Reducer<Text, Text, Text, Text>.Context context) throws IOException, InterruptedException {
		String firstvalue = values.iterator().next().toString();
		System.out.println("firstvalue " + firstvalue);
		String[] tab = firstvalue.split("--");

			String provider = tab[0];
			String priceStr = tab[1];
			float price, min = Float.parseFloat(priceStr);
			for(Text value  : values){
				tab = value.toString().split("--");
				priceStr = tab[1];
				price = Float.parseFloat(priceStr);
				if(price < min){
					provider = tab[0];
					min = price;
				}
			}
			
			context.write(new Text(key), new Text(provider + "--" + min));
		
		
		
	}

}
