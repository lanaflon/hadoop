package com.intellitech.healthcare.cheapestProvider;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import au.com.bytecode.opencsv.CSVParser;

public class ProcedureCheapestProviderMapper extends Mapper<LongWritable, Text, Text, Text> {


	@Override
	protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, Text>.Context context)
			throws IOException, InterruptedException {

		CSVParser parser = new CSVParser();
		String[] parts = parser.parseLine(value.toString());

		String procedure = new String(parts[0]);
		if (!procedure.equals("DRG Definition")) // Don't process header
		{

			String provider = new String(parts[2]);
			
			
			String price = new String(parts[10]);
			price = price.substring(1); // Remove "$"
			price = price.replace(",", ""); // Remove internal commas
			
			
			/*
			 * Long keyLong = 0L; keyLong = Long.parseLong(parts[0].split(" - "
			 * )[0]);
			 */

			if(!provider.equals("")){
				context.write(new Text(procedure), new Text(provider + "--" + price));
			}

		}
	}

}
