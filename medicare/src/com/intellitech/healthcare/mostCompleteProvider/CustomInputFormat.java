package com.intellitech.healthcare.mostCompleteProvider;

import java.io.IOException;

import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

public class CustomInputFormat extends FileInputFormat<ProviderState , ProcedurePrice> {



	@Override
	public RecordReader<ProviderState, ProcedurePrice> createRecordReader(InputSplit split, TaskAttemptContext context)
			throws IOException, InterruptedException {
		CustomReader cr = new CustomReader();
		cr.initialize(split, context);
		return cr;
	}

}
