package com.intellitech.healthcare.mostCompleteProvider;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.LineRecordReader;

import au.com.bytecode.opencsv.CSVParser;

public class CustomReader extends RecordReader<ProviderState, ProcedurePrice> {

	private ProviderState providerState;
	private ProcedurePrice procedurePrice;
	private LineRecordReader lineRecordReader;

	@Override
	public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
		lineRecordReader = new LineRecordReader();
		lineRecordReader.initialize(split, context);

	}

	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		if (!lineRecordReader.nextKeyValue()) {
			return false;
		}
		Text value = lineRecordReader.getCurrentValue();
		if (value.toString().startsWith("DRG Definition")) {
			lineRecordReader.nextKeyValue();
			value = lineRecordReader.getCurrentValue();
		}
		
		CSVParser parser = new CSVParser();
		String[] valueArr = parser.parseLine(value.toString());
		providerState = new ProviderState(new Text(valueArr[2]),new Text(valueArr[5]));
		
		String tc = new String(valueArr[10]);
		tc = tc.substring(1); // Remove "$"
		tc = tc.replace(",", ""); // Remove internal commas
		procedurePrice = new ProcedurePrice(new Text(valueArr[0]),new FloatWritable(Float.parseFloat(tc)));
		return true;
	}

	@Override
	public ProviderState getCurrentKey() throws IOException, InterruptedException {
		return providerState;
	}

	@Override
	public ProcedurePrice getCurrentValue() throws IOException, InterruptedException {
		return procedurePrice;
	}

	@Override
	public float getProgress() throws IOException, InterruptedException {
		return lineRecordReader.getProgress();
	}

	@Override
	public void close() throws IOException {
		lineRecordReader.close();
	}

}
