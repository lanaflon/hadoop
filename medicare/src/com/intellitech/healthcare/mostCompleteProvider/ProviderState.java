package com.intellitech.healthcare.mostCompleteProvider;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class ProviderState implements WritableComparable<ProviderState> {
	private Text provider;
	private Text state;

	public ProviderState() {
		provider = new Text();
		state = new Text();
	}

	public ProviderState(Text provider, Text state) {
		this.provider = provider;
		this.state = state;
	}

	public Text getProvider() {
		return provider;
	}

	public Text getState() {
		return state;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		provider.readFields(in);
		state.readFields(in);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		provider.write(out);
		state.write(out);
	}

	@Override
	public int compareTo(ProviderState y) {
		int codeCmp = this.getProvider().compareTo(y.getProvider());
		if (codeCmp != 0) {
			return codeCmp;
		} else {
			return this.getState().compareTo(y.getState());
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((provider == null) ? 0 : provider.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProviderState other = (ProviderState) obj;
		if (provider == null) {
			if (other.provider != null)
				return false;
		} else if (!provider.equals(other.provider))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return provider + " " + state;
	}

}
