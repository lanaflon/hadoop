package com.intellitech.healthcare.mostCompleteProvider;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class ProviderSortComparator extends WritableComparator {

	private static Text.Comparator comparator = new Text.Comparator();

	public ProviderSortComparator() {
		super(ProviderState.class, true);
	}

	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		//System.out.println("----> Calling  sorting");
		ProviderState x = (ProviderState) a;
		ProviderState y = (ProviderState) b;
		int codeCmp = comparator.compare(x.getProvider(), y.getProvider());
		if (codeCmp != 0) {
			return codeCmp;
		} else {
			return comparator.compare(x.getState(), y.getState());
		}
	}

}
