package com.intellitech.healthcare.mostCompleteProvider;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class MostCompleteProviderMain extends Configured implements Tool{

	@Override
	public int run(String[] args) throws Exception {
		
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "MostCompleteProviderMain");
		job.setJarByClass(MostCompleteProviderMain.class);
		
		job.setInputFormatClass(CustomInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		TextInputFormat.setInputPaths(job, new Path(args[0]));
		TextOutputFormat.setOutputPath(job, new Path(args[1]));
		
		
		job.setMapperClass(MostCompleteProviderMapper.class);
		job.setReducerClass(MostCompleteProviderReducer.class);
		//job.setCombinerClass(MostCompleteProviderCombiner.class);
		
		job.setSortComparatorClass(ProviderSortComparator.class);
		job.setGroupingComparatorClass(ProviderGroupComparator.class);
		
		job.setMapOutputKeyClass(ProviderState.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(ProviderState.class);
		job.setOutputValueClass(Text.class);
		
		Path outputFile = new Path(args[1]);
		outputFile.getFileSystem(conf).delete(outputFile, true);
		return job.waitForCompletion(true) ? 0 : 1;
	}
	
	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new MostCompleteProviderMain(), new String[] { "hdfs://localhost:9000/medicare/Inpatient_big.csv",
				"hdfs://localhost:9000/medicare/output/MostCompleteProviderMain3" });
		
		System.exit(exitCode);

	}
}
