package com.intellitech.healthcare.mostCompleteProvider;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MostCompleteProviderReducer extends Reducer<ProviderState, Text, ProviderState, Text> {

	@Override
	protected void reduce(ProviderState key, Iterable<Text> values,
			Reducer<ProviderState, Text, ProviderState, Text>.Context context)
			throws IOException, InterruptedException {

		int counter = 0;
		/*Iterator<Text> it = values.iterator();
		while (it.hasNext()) {
		  Text value = it.next();
		
		  
		}*/
		System.out.println();
		for (Text value : values){
			counter++;
		    
		}
		System.out.println(key + " --> " + counter);
		context.write(key, new Text(counter+""));

	}

}
