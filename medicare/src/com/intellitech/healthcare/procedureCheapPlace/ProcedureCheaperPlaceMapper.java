package com.intellitech.healthcare.procedureCheapPlace;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import au.com.bytecode.opencsv.CSVParser;

public class ProcedureCheaperPlaceMapper extends Mapper<LongWritable, Text, Text, FloatWritable> {

	private Long procedureCode;

	@Override
	protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, FloatWritable>.Context context)
			throws IOException, InterruptedException {
	
			CSVParser parser = new CSVParser();
			String[] parts = parser.parseLine(value.toString());

			String state = new String(parts[5]);
			Long keyLong = 0L;
			keyLong = Long.parseLong(parts[0].split(" - ")[0]);
			procedureCode = context.getConfiguration().getLong("procedureCode", 0L);
			if (procedureCode.equals(keyLong)) // Don't process header
			{

				String tc = new String(parts[10]);
				tc = tc.substring(1);
				tc = tc.replace(",", "");
				//System.out.println("tc " + tc + " state " + state);
				context.write(new Text(state), new FloatWritable(Float.parseFloat(tc)));
			}
		
	}

}
