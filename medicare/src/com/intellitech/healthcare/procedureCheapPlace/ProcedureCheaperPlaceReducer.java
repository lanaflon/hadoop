package com.intellitech.healthcare.procedureCheapPlace;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class ProcedureCheaperPlaceReducer extends Reducer<Text, FloatWritable, Text, FloatWritable> {

	@Override
	protected void reduce(Text key, Iterable<FloatWritable> values,
			Reducer<Text, FloatWritable, Text, FloatWritable>.Context context) throws IOException, InterruptedException {
		float min = values.iterator().next().get();
		String state = key.toString();
		for(FloatWritable value  : values){
			if(value.get() < min){
				min = value.get();
			}
		}
		
		context.write(new Text(state), new FloatWritable(min));
		
	}

}
