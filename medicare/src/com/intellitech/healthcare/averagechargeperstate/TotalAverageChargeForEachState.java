package com.intellitech.healthcare.averagechargeperstate;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class TotalAverageChargeForEachState extends Configured implements Tool{

	@Override
	public int run(String[] args) throws Exception {
		
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "TotalAverageChargeForEachState");
		job.setJarByClass(TotalAverageChargeForEachState.class);
		
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		TextInputFormat.setInputPaths(job, new Path(args[0]));
		TextOutputFormat.setOutputPath(job, new Path(args[1]));

		job.setMapperClass(TotalAverageChargeForEachStateMapper.class);
		job.setReducerClass(TotalAverageChargeForEachStateReducer.class);
		job.setCombinerClass(TotalAverageChargeForEachStateReducer.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(FloatWritable.class);
		
		Path outputFile = new Path(args[1]);
		outputFile.getFileSystem(conf).delete(outputFile, true);
		return job.waitForCompletion(true) ? 0 : 1;
	}
	
	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new TotalAverageChargeForEachState(), new String[] { "hdfs://localhost:9000/healthcare/inputs/*",
				"hdfs://localhost:9000/healthcare/TotalAverageChargeForEachState/output" });
		
		System.exit(exitCode);

	}
}