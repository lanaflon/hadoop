package com.intellitech.healthcare.averagechargeperprocedure;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class TotalAverageChargeForEachProcedure extends Configured implements Tool{

	@Override
	public int run(String[] args) throws Exception {
		
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "TotalAverageChargeForEachProcedure");
		job.setJarByClass(TotalAverageChargeForEachProcedure.class);
		
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		TextInputFormat.setInputPaths(job, new Path(args[0]));
		TextOutputFormat.setOutputPath(job, new Path(args[1]));

		job.setMapperClass(TotalAverageChargeForEachProcedureMapper.class);
		job.setReducerClass(TotalAverageChargeForEachProcedureReducer.class);
		job.setCombinerClass(TotalAverageChargeForEachProcedureReducer.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(FloatWritable.class);
		
		Path outputFile = new Path(args[1]);
		outputFile.getFileSystem(conf).delete(outputFile, true);
		return job.waitForCompletion(true) ? 0 : 1;
	}
	
	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new TotalAverageChargeForEachProcedure(), new String[] { "hdfs://localhost:9000/healthcare/inputs/*",
				"hdfs://localhost:9000/healthcare/TotalAverageChargeForEachProcedure/output" });
		
		System.exit(exitCode);

	}
}
