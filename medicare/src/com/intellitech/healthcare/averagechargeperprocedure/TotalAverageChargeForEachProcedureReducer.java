package com.intellitech.healthcare.averagechargeperprocedure;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class TotalAverageChargeForEachProcedureReducer extends Reducer<Text, FloatWritable, Text, FloatWritable> {
	@Override
	protected void reduce(Text key, Iterable<FloatWritable> values,
			Reducer<Text, FloatWritable, Text, FloatWritable>.Context context)
			throws IOException, InterruptedException {

		// Define a local variable totalPrice
		float total_charge = 0;
		long num_entries = 0;

		for (FloatWritable value : values) {
			total_charge += value.get();
			num_entries++;
		}

		// Write the symbol and total volume
		context.write(key, new FloatWritable(total_charge / num_entries));
	}
}
