package com.intellitech.healthcare.averagechargeperprocedure;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import au.com.bytecode.opencsv.CSVParser;


public class TotalAverageChargeForEachProcedureMapper extends Mapper<LongWritable, Text, Text, FloatWritable> {

	@Override
	protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, FloatWritable>.Context context)
			throws IOException, InterruptedException {

		CSVParser parser = new CSVParser();
		String[] parts = parser.parseLine(value.toString());

		String procedure = new String(parts[0]);
		if (!procedure.equals("DRG Definition")) // Don't process header
		{

			String tc = new String(parts[10]);
			tc = tc.substring(1); // Remove "$"
			tc = tc.replace(",", ""); // Remove internal commas

			context.write(new Text(procedure), new FloatWritable(Float.parseFloat(tc)));
		}
	}

}
