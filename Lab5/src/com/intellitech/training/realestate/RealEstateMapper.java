package com.intellitech.training.realestate;

import java.io.IOException;

import org.apache.hadoop.mapreduce.Mapper;

import com.intellitech.training.realestate.model.AddressCityWritable;
import com.intellitech.training.realestate.model.PostalCodePriceWritable;

public class RealEstateMapper
		extends Mapper<PostalCodePriceWritable,AddressCityWritable,PostalCodePriceWritable,AddressCityWritable> {

	@Override
	protected void map(PostalCodePriceWritable key,AddressCityWritable value,
			Mapper<PostalCodePriceWritable, AddressCityWritable,PostalCodePriceWritable ,AddressCityWritable >.Context context)
					throws IOException, InterruptedException {
		context.write(key, value);
	}

}
