package com.intellitech.training.realestate.model;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

public class AddressCityWritable implements Writable{

	private Text address;
	private IntWritable cityId;
	
	public AddressCityWritable() {
		this.address = new Text();
		this.cityId = new IntWritable();
	}
	
	public AddressCityWritable(String adresse, String numVille) {
		this.address = new Text(adresse);
		this.cityId = new IntWritable(Integer.parseInt(numVille));
	}
	
	public Text getAddress() {
		return address;
	}

	public void setAddress(Text address) {
		this.address = address;
	}

	@Override
	public int hashCode() {
		return address.hashCode() + cityId.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AddressCityWritable) {
			AddressCityWritable adresseVilleWritable = (AddressCityWritable) obj;
			return this.getCityId().equals(adresseVilleWritable.getCityId())
					&& this.getAddress().equals(adresseVilleWritable.getAddress());
		}
		return false;
	}

	@Override
	public String toString() {
		return this.cityId.toString() + " " + this.address.toString();
	}
	
	@Override
	public void write(DataOutput dataOutput) throws IOException {
		address.write(dataOutput);
		cityId.write(dataOutput);
	}

	@Override
	public void readFields(DataInput dataInput) throws IOException {
		address.readFields(dataInput);
		cityId.readFields(dataInput);
	}

	public IntWritable getCityId() {
		return cityId;
	}

	public void setCityId(IntWritable cityId) {
		this.cityId = cityId;
	}


}
