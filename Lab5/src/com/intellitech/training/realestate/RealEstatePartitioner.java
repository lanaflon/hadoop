package com.intellitech.training.realestate;

import org.apache.hadoop.mapreduce.Partitioner;

import com.intellitech.training.realestate.model.AddressCityWritable;
import com.intellitech.training.realestate.model.PostalCodePriceWritable;


public class RealEstatePartitioner extends Partitioner<PostalCodePriceWritable,AddressCityWritable >{

	@Override
	public int getPartition(PostalCodePriceWritable key,AddressCityWritable value, int numPartitions) {
		if(Integer.parseInt(key.getCodePostal().toString())< 3051){
			return 0;
		}
		return 1;
	}
}
