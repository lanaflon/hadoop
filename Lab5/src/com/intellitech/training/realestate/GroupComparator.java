package com.intellitech.training.realestate;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

import com.intellitech.training.realestate.model.PostalCodePriceWritable;

public class GroupComparator extends WritableComparator{
	
	private static IntWritable.Comparator comparator = new 
			IntWritable.Comparator();
	
	protected GroupComparator() {
		super(PostalCodePriceWritable.class,true);
	}
	
	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		PostalCodePriceWritable a1=(PostalCodePriceWritable) a;
		PostalCodePriceWritable b1=(PostalCodePriceWritable) b;
		return comparator.compare(a1.getCodePostal(), b1.getCodePostal());
	}
}
