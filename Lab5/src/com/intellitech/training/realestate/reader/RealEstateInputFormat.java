package com.intellitech.training.realestate.reader;

import java.io.IOException;

import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import com.intellitech.training.realestate.model.AddressCityWritable;
import com.intellitech.training.realestate.model.PostalCodePriceWritable;

public class RealEstateInputFormat extends FileInputFormat<PostalCodePriceWritable, AddressCityWritable>{

	@Override
	public RecordReader<PostalCodePriceWritable, AddressCityWritable> createRecordReader(InputSplit inputSplit,
			TaskAttemptContext context) throws IOException, InterruptedException {
		RealEstateRecordReader realEstateRecordReader=new RealEstateRecordReader();
		realEstateRecordReader.initialize(inputSplit, context);
		return realEstateRecordReader;
	}
}
