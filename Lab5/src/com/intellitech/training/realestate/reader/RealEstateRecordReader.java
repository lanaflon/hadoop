package com.intellitech.training.realestate.reader;

import java.io.IOException;

import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.LineRecordReader;

import com.intellitech.training.realestate.model.AddressCityWritable;
import com.intellitech.training.realestate.model.PostalCodePriceWritable;

public class RealEstateRecordReader extends RecordReader<PostalCodePriceWritable,AddressCityWritable >{

	LineRecordReader lineRecordReader;
	PostalCodePriceWritable key;
	AddressCityWritable value;
	TaskAttemptContext context;
	
	@Override
	public void initialize(InputSplit inputSplit, TaskAttemptContext context) throws IOException, InterruptedException {
		this.context = context;
		lineRecordReader = new LineRecordReader();
		lineRecordReader.initialize(inputSplit, context);
	}

	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		context.getCounter("UserCounters","numberOfnextKeyValueAppel").increment(1);
		if (!lineRecordReader.nextKeyValue()) {
			return false;
		}
		context.getCounter("UserCounters","numberOfLines").increment(1);
		
		
		String line = lineRecordReader.getCurrentValue().toString(); 

		String[] allFields = line.split(",");
		
		key = new PostalCodePriceWritable(allFields[2],allFields[3]);
		value = new AddressCityWritable(allFields[0],allFields[1]);
		return true;
	}

	@Override
	public PostalCodePriceWritable getCurrentKey() throws IOException, InterruptedException {
		return key;
	}

	@Override
	public AddressCityWritable getCurrentValue() throws IOException, InterruptedException {
		return value;
	}

	@Override
	public float getProgress() throws IOException, InterruptedException {
		return lineRecordReader.getProgress();
	}

	@Override
	public void close() throws IOException {
		lineRecordReader.close();
	}
}
