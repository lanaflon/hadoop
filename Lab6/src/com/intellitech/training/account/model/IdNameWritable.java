package com.intellitech.training.account.model;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class IdNameWritable implements WritableComparable<IdNameWritable>{

	private LongWritable id;
	private Text name;
	 
	public IdNameWritable() {
		this.id = new LongWritable();
		this.name = new Text();
	}
	
	public IdNameWritable(String id, String valeur) {
		this.id = new LongWritable(Long.parseLong(id));
		this.name = new Text(valeur);
	}
	
	public LongWritable getId() {
		return id;
	}

	public void setId(LongWritable id) {
		this.id = id;
	}


	@Override
	public int hashCode() {
		return id.hashCode() + name.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IdNameWritable) {
			IdNameWritable doubleIntWritable = (IdNameWritable) obj;
			return this.getId().equals(doubleIntWritable.getId())
					&& this.name.equals(doubleIntWritable.name);
		}
		return false;
	}

	@Override
	public String toString() {
		return " postal code :  "+ id.toString()+ " price :  "+ name.toString();
	}
	@Override
	public void readFields(DataInput dataInput) throws IOException {
		id.readFields(dataInput);
		name.readFields(dataInput);
	}

	@Override
	public void write(DataOutput dataOutput) throws IOException {
		id.write(dataOutput);
		name.write(dataOutput);
	}

	@Override
	public int compareTo(IdNameWritable iDEmailWritable) {
	int codeCmp=id.compareTo(iDEmailWritable.getId());
	if(codeCmp!=0){
		return codeCmp;
	}else {
		return name.compareTo(iDEmailWritable.getName());
	}
		
	}

	public Text getName() {
		return name;
	}

	public void setName(Text name) {
		this.name = name;
	}
}
