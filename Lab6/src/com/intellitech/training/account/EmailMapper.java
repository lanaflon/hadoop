package com.intellitech.training.account;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.intellitech.training.account.model.IdEmailWritable;

public class EmailMapper
		extends Mapper<LongWritable,IdEmailWritable,LongWritable,Text> {

	@Override
	protected void map(LongWritable key, IdEmailWritable value,
			Mapper<LongWritable, IdEmailWritable, LongWritable, Text>.Context context)
			throws IOException, InterruptedException {
		System.out.println("email map " + value.getEmail());
		context.write(value.getId(), value.getEmail());
	}



}
