package com.intellitech.training.account;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class AccountReducer extends Reducer<LongWritable, Text,LongWritable, Text>{

	@Override
	protected void reduce(LongWritable key, Iterable<Text> values,
			Reducer<LongWritable, Text, LongWritable, Text>.Context context) throws IOException, InterruptedException {
		String result = "";
		for(Text value : values){
			result+= " : " + value.toString();
		}
		System.out.println("result " + result);
		context.write(key,new Text(result));
	}


	
}
