package com.intellitech.training.account.reader;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import com.intellitech.training.account.model.IdEmailWritable;

public class EmailInputFormat extends FileInputFormat<LongWritable, IdEmailWritable>{

	@Override
	public RecordReader<LongWritable, IdEmailWritable> createRecordReader(InputSplit inputSplit, TaskAttemptContext context)
			throws IOException, InterruptedException {
		EmailRecordReader emailRecordReader=new EmailRecordReader();
		emailRecordReader.initialize(inputSplit, context);
		return emailRecordReader;
	}


}
