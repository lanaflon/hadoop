package com.intellitech.training.account.reader;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.LineRecordReader;

import com.intellitech.training.account.model.IdEmailWritable;

public class EmailRecordReader extends RecordReader<LongWritable,IdEmailWritable >{

	LineRecordReader lineRecordReader;
	LongWritable key;
	IdEmailWritable value;
	TaskAttemptContext context;
	
	@Override
	public void initialize(InputSplit inputSplit, TaskAttemptContext context) throws IOException, InterruptedException {
		this.context = context;
		lineRecordReader = new LineRecordReader();
		lineRecordReader.initialize(inputSplit, context);
	}

	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		
		if (!lineRecordReader.nextKeyValue()) {
			return false;
		}	
		
		String line = lineRecordReader.getCurrentValue().toString(); 
		System.out.println("line " + line);
		String[] allFields = line.split("\t");
		
		String[] values  = allFields[1].split(" ");
		key = new LongWritable(Long.parseLong(allFields[0]));
		value = new IdEmailWritable(allFields[0],values[0]);
		return true;
	}

	@Override
	public LongWritable getCurrentKey() throws IOException, InterruptedException {
		return key;
	}

	@Override
	public IdEmailWritable getCurrentValue() throws IOException, InterruptedException {
		return value;
	}

	@Override
	public float getProgress() throws IOException, InterruptedException {
		return lineRecordReader.getProgress();
	}

	@Override
	public void close() throws IOException {
		lineRecordReader.close();
	}
}
