package com.intellitech.training.account.reader;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import com.intellitech.training.account.model.IdNameWritable;

public class NameInputFormat extends FileInputFormat<LongWritable, IdNameWritable>{

	@Override
	public RecordReader<LongWritable, IdNameWritable> createRecordReader(InputSplit inputSplit, TaskAttemptContext context)
			throws IOException, InterruptedException {
		NameRecordReader nameRecordReader=new NameRecordReader();
		nameRecordReader.initialize(inputSplit, context);
		return nameRecordReader;
	}
}
