package com.intellitech.training.account;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.intellitech.training.account.reader.EmailInputFormat;
import com.intellitech.training.account.reader.NameInputFormat;


public class AccountJob extends Configured implements Tool{

	@Override
	public int run(String[] args) throws Exception {
		
		Job job = Job.getInstance(getConf(), "lab6");
		job.setJarByClass(getClass());
	
		job.setOutputFormatClass(TextOutputFormat.class);
		TextOutputFormat.setOutputPath(job, new Path(args[2]));
		

		MultipleInputs.addInputPath(job, new Path(args[0]), NameInputFormat.class, NameMapper.class);
		MultipleInputs.addInputPath(job, new Path(args[1]), EmailInputFormat.class, EmailMapper.class);

		job.setReducerClass(AccountReducer.class);
		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Text.class);
		//job.setNumReduceTasks(2);
		

		
		Path output = new Path(args[2]);
		output.getFileSystem(job.getConfiguration()).delete(output, true);
		
		int r = job.waitForCompletion(true) ? 0 : 1 ;

		return r;	
	}
	
	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new AccountJob(),
				new String[] { "hdfs://localhost:9000/training/lab6/TP6inputs/first.txt",
						"hdfs://localhost:9000/training/lab6/TP6inputs/second.txt",
						"hdfs://localhost:9000/training/lab6/output/" 
					});
		System.exit(exitCode);
	}

}
