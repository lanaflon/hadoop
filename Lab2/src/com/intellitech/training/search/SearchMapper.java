package com.intellitech.training.search;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class SearchMapper extends Mapper<LongWritable,Text,Text,IntWritable>{

	private String searchString;
	
	@Override
	protected void setup(Mapper<LongWritable, Text, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {
		searchString =
				context.getConfiguration().get("searchingWord");
	}
	
	@Override
	protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, IntWritable>.Context context)
			throws IOException, InterruptedException {
		
		String [] words = value.toString().split(" ");
		int counter = 0;
		
		for (int i=0;  i< words.length;i++) {
			if(searchString.equals(words[i])){
				counter++;
			}
		}
		
		context.write(new Text(searchString), new IntWritable(counter));
	}
}
