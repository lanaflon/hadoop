package com.intellitech.training.temperature.reader;

import java.io.IOException;

import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import com.intellitech.training.temperature.model.CityTemperature;
import com.intellitech.training.temperature.model.CustomDate;

public class SousseInputFormat extends FileInputFormat<CustomDate,CityTemperature> {

	@Override
	public RecordReader<CustomDate,CityTemperature> createRecordReader(InputSplit inputSplit,
			TaskAttemptContext context) throws IOException, InterruptedException {
		SousseRecordReader sousseRecordReader = new SousseRecordReader();
		sousseRecordReader.initialize(inputSplit, context);
		return sousseRecordReader;
	}
}
