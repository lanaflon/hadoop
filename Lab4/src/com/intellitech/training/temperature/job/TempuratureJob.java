package com.intellitech.training.temperature.job;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.intellitech.training.temperature.model.CustomDate;
import com.intellitech.training.temperature.reader.SfaxInputFormat;
import com.intellitech.training.temperature.reader.SousseInputFormat;

public class TempuratureJob extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {

		if (args.length != 3) {
			System.err.println("error");
			return -1;
		}

		Configuration conf = getConf();

		Job job = Job.getInstance(conf, "lab4");
		job.setJarByClass(TempuratureJob.class);

		MultipleInputs.addInputPath(job, new Path(args[0]), SfaxInputFormat.class, TempuratureMapper.class);
		MultipleInputs.addInputPath(job, new Path(args[1]), SousseInputFormat.class, TempuratureMapper.class);

		job.setOutputFormatClass(TextOutputFormat.class);
		TextOutputFormat.setOutputPath(job, new Path(args[2]));

		job.setReducerClass(TempuratureReducer.class);
		job.setOutputKeyClass(CustomDate.class);
		job.setOutputValueClass(IntWritable.class);

		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		String sfaxInputPath = "hdfs://localhost:9000/training/lab4/inputs/sfax.txt";
		String sousseInputPath = "hdfs://localhost:9000/training/lab4/inputs/sousse.txt";
		String outputPath = "hdfs://localhost:9000/training/lab4/output";
		int exitCode = ToolRunner.run(new TempuratureJob(),
				new String[] { sfaxInputPath, sousseInputPath, outputPath });
		System.exit(exitCode);

	}
}
