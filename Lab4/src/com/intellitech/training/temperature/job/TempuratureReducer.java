package com.intellitech.training.temperature.job;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

import com.intellitech.training.temperature.model.CustomDate;

public class TempuratureReducer 
	extends Reducer<CustomDate,IntWritable,CustomDate,IntWritable>{
	
	@Override
	protected void reduce(CustomDate key, Iterable<IntWritable> values,
		Reducer<CustomDate, IntWritable, CustomDate, IntWritable>.Context context)
				throws IOException, InterruptedException {
		int sum = 0;
		int nbr =0 ;
		
		for (IntWritable value : values) {
			nbr++;
			sum=sum+value.get();
		}

		context.write(key, new IntWritable(sum/nbr));
	}
}
