package com.intellitech.training.temperature.job;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Mapper;

import com.intellitech.training.temperature.model.CityTemperature;
import com.intellitech.training.temperature.model.CustomDate;

public class TempuratureMapper extends Mapper<CustomDate,CityTemperature ,CustomDate,IntWritable>{
	@Override
	protected void map(CustomDate key, CityTemperature value, Mapper<CustomDate,CityTemperature ,CustomDate ,IntWritable >.Context context)
			throws IOException, InterruptedException {
		
		context.write(key,value.getTemperature());
	}

}
